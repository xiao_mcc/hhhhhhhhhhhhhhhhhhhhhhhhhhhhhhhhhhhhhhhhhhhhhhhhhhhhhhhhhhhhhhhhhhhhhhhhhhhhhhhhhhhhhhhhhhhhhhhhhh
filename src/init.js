
export default function(video,success,errBack){
   
    let navigator =navigator.plus
    // Grab elements, create settings, etc.
    var mediaConfig =  {
        video: { facingMode: "user" }
    };
    // Put video listeners into place
    if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        console.log(1)
        navigator.mediaDevices.getUserMedia(mediaConfig).then(function(stream) {
            // video.src = window.URL.createObjectURL(stream);
            video.srcObject = stream;
            video.play();
            success();
        });
    }
    /* Legacy code below! */
    else if(navigator.getUserMedia) { // Standard
        console.log(2)
        navigator.getUserMedia(mediaConfig, function(stream) {
          
            video.src = stream;
            video.play(); 
            success();
        }, errBack);
    } else if(navigator.mozGetUserMedia) { 
        // Mozilla-prefixed
        console.log(3)
        navigator.mozGetUserMedia(mediaConfig, function(stream){
          
            video.src = window.URL.createObjectURL(stream);
            video.play();
            success();
        }, errBack);
    } else if(navigator.msGetUserMedia) { // Mozilla-prefixed
        console.log(4)
        navigator.msGetUserMedia(mediaConfig, function(stream){
          
            video.src = window.URL.createObjectURL(stream);
            video.play();
            success();
            
        }, errBack);
    } else if(navigator.webkitGetUserMedia) { // WebKit-prefixed
        console.log(5)
        navigator.webkitGetUserMedia(mediaConfig, function(stream){
          
            video.src = window.webkitURL.createObjectURL(stream);
            video.play();
            success();
        }, errBack);
    }
    console.log( JSON.stringify(navigator),888)
}

