import cml from './clmtrackr.module'
import "./jquery";
import install from "./init";
import css from "./css.css";
import tree from "./tree.js";
// import permission from "./permission";

function getQueryVariable(variable) {
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if (pair[0] == variable) {
                return pair[1];
            }
        }
        return (false);
}
   
 
document.addEventListener("plusready", function () {
    plus.android.requestPermissions(['android.permission.CAMERA'], function (e) {
       console.log(e)
       if(e.granted[0]){
        crea(getQueryVariable('type'));
       }else{

        alert('请打开应用相机权限!')
        gotoAppPermissionSetting()
        
       }
    })


    // var cmr = plus.camera.getCamera();
	// var res = cmr.supportedImageResolutions[0];
	// var fmt = cmr.supportedImageFormats[0];
	// console.log("Resolution: "+res+", Format: "+fmt);
	// cmr.captureImage( function( path ){
	// 		alert( "Capture image success: " + path );  
	// 	},
	// 	function( error ) {
	// 		alert( "Capture image failed: " + error.message );
	// 	},
	// 	{resolution:res,format:fmt}
	// );

    var AVCaptureDevice = plus.ios.import("AVCaptureDevice");
	var authStatus = AVCaptureDevice.authorizationStatusForMediaType('vide');
	if (authStatus == 3) {
       
        crea(getQueryVariable('type'));
	} else {
        alert('请打开应用相机权限!')
        gotoAppPermissionSetting()
        
	}
	plus.ios.deleteObject(AVCaptureDevice);

    // 跳转到**应用**的权限页面
function gotoAppPermissionSetting() {
	if (plus.os.name == "iOS") {
		var UIApplication = plus.ios.import("UIApplication");
		var application2 = UIApplication.sharedApplication();
		var NSURL2 = plus.ios.import("NSURL");
		// var setting2 = NSURL2.URLWithString("prefs:root=LOCATION_SERVICES");		
		var setting2 = NSURL2.URLWithString("app-settings:");
		application2.openURL(setting2);

		plus.ios.deleteObject(setting2);
		plus.ios.deleteObject(NSURL2);
		plus.ios.deleteObject(application2);
	} else {
		// console.log(plus.device.vendor);
		var Intent = plus.android.importClass("android.content.Intent");
		var Settings = plus.android.importClass("android.provider.Settings");
		var Uri = plus.android.importClass("android.net.Uri");
		var mainActivity = plus.android.runtimeMainActivity();
		var intent = new Intent();
		intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
		var uri = Uri.fromParts("package", mainActivity.getPackageName(), null);
		intent.setData(uri);
		mainActivity.startActivity(intent);
	}
}
}, false);



$('head').append(`<style>${css}</style>`);

function crea(type) {
    var type = JSON.parse(decodeURIComponent(type));
    var width = $(window).width() * 0.8;
    var height = $(window).height();
    var poster="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD//gAPTGF2YzU3LjI0LjEwMv/bAEMADQkKCwoIDQsKCw4ODQ8TIBUTEhITJxweFyAuKTEwLiktLDM6Sj4zNkY3LC1AV0FGTE5SU1IyPlphWlBgSlFST//bAEMBDg4OExETJhUVJk81LTVPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT//AABEIAmICYgMBIgACEQEDEQH/xAAbAAEAAwEBAQEAAAAAAAAAAAAABQYHBAIDAf/EAEgQAQABAwECCQcKBAQFBQEAAAABAgMEBQYREhMhMUFRYXGRBxQigaGxwRUjJDJCQ1JTctEzNGKCRGPC4UVzg5OiVJKy0vB0/8QAFwEBAQEBAAAAAAAAAAAAAAAAAAECA//EABwRAQEBAAMBAQEAAAAAAAAAAAABEQISMSFRQf/aAAwDAQACEQMRAD8A0kAAAAAAAAAAAAAAAAAAAAAAAAAAcGoazpmm/wA7nWrVX4eFvq8I5VZzvKLg299OBi3cir8VyYop+Mrgur8mqmKd9XJHXPJHiyjO271zK38TXbxqf8qnl8Z3oDLz83Nq4WXlX7//ADK5n2L1Gx5e0WjYX8xqOPFXVFXCq8IQ+T5QNFs/wacm/wDpt8GPGWWC9YL/AJHlJn/C6V67t74RCOveUPWa/wCHaxLP9k1T7ZVEXILBd212huf46KP02qY+Dlr2m1259bVcn1VRCJDEd9Wt6tX9bU8v/vVPn8qaj/6/L/79f7uQB1/Kmpf+vzP+/X+71TrGqUfV1HL/AO9VLiASlG0euUfV1XLj+/e6bW2O0Fv/AIjXX+q3TV8EEGC1Wdv9ct/xPNbv6rW73TCQx/KTfj+Y0y1X227s0+yVFDIrTcbyi6Xc/mMbKseqKoTGNtXoGVu4vUbVFXVd30T7WNCdYN7tXrd+nhWa6LlPXTVFUeL6MEs3r2PVwse7ctVR026pplN4e2Ou4n+M4+nqvUxV7edOo2AUDB8o8clOo6fu/qsVb/ZP7rNp21Oi6jupx863RXP3d30KvbzplEyAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAjtW1zTtHt8LPyaaaui3HpVz3UgkXPl5uLg2eNy8m3Yo67lUR4dbPdX8oWZf30aVajFo/Mubqq57o5oU/Jyr+Xem7k37l2uftXKpqn/ZrqNF1Tyh4VjhUaZj15NX5lfoUfvKo6ntbrWo76bmVNq1P3dn0Y9c88oMayBz+l9rr6QFQAAAAAAAAAAAAAAAAAAAAABI6druqaXV9CzrtFP5czwqfCVt0vyjTyUarh/8AVse+aZUETIrbtM1vTNVp+hZVu5V+X9WqO+lIsBpmaKqaqapiqOaYndMd0rNpG3GrYG6jIqjMsR0XfrRHZV+7N4jWBAaNtbpOrbrdN3zfIn7q9upmZ7J5pT7IAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+d69bsWaruRcot0URvmqqYiI75B9HBqmr4Gk2eNz8mi31U89VXdHSp+v7f8HhWNEp39HnNyOT+2n91Eyci/l3pv5V2u7dr56qp3zLU4i2a3t7m5fCtaVT5pa/MndNyfX0KhduV3rk13K5rrnlmqqZmZ75eRrMABUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFg0Xa/VNK3UVXPOcePur0zO6OyrnhXxBseh7U6ZrPBos18Vk/k3d0VeqelOMA5vSW3QNuc3A3WNR35mPzb5n5ymOyemO9m8fxWpDj0zVMLVcfj8C/FynpjmqpnqmOiXYyAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA81VU0UzVVVEUxG+ZnkiI71C2m263cPD0Orsqyf/pHxWTRY9odp8DQ6eDcq47KqjksUTy99U9EMw1rX9Q1u9wsu76ET6NmnfFFPq6ZRtdddy5Ny5VNdczvmZnfMz1zLy1JgANIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA6MHOytOyIyMK/Xaux00z7Jjpho+ze3GNn8DG1PgY2RzRc5rdc/6ZZgJZqt/GU7NbZZWk8DGzeHkYfN1124/pnpjsadg5uNn4tGTiXYu2q+aY90x0MWYOgBAAAAAAAAAAAAAAAAAAAAAAAAAAAc+dnY2nYteVl3Yt2qOeZ6eyI6Zc2tazh6Jh+cZdfZRbj61yeqIZJruuZmuZXG5NW6iP4dqPq0R2dfesmiQ2n2tytbqmxZ32MLfyW9/pV9tU/D3q4DYAKgAAAAAAAAAAAAAAJ7SdkdZ1TdXTY83sT95f30+Ec8rfp3k906xuqz793Jr6o9Cn95TYrMndjaNqmX6WNp2Tcp64tTu8WyYelabgU/RMGxa7Yojf4u1nsMgtbFbQXP8DFH/Mu0x7N7pp2A138OLH/W/wBmrB2oymrYDXY+zjT/ANb/AGc13YraC3/gYr/5d2mfZva+HajD8nRtUxP5nTsm3T1zamY8XB/S39xZmkabn/zeDYu9s0RwvHnOww0abqPk906/vqwL93Fr6p9Onw54VDVtkdZ0uma6rHnFiPvbG+qO+Y54a2CBAVAAAAAAAAAAAAAABI6LrWbouVx+JX6M/wAS1P1a46pjrRwg2jQNoMLXcfh49XAu0R85Zr+tT29sdqXYPh5d/ByqMnEuzau0TviqPd2w1TZXarH1u3FjI3Wc+mOWjoubumn9mLFWQBAAAAAAAAAAAAAAAAAAAAAAARO0Ou4uhYPG3vTu174tWYnlrn4Q/NoddxdCweNvendr3xatRPLXPwhkOo5+TqeZXlZd2bl2vwiOiIjohZNHrVNUytWzq8rNr4dc80fZop6ojohxg2gAoAAAAAAAAAAAAD3ZsXsi9RYx6K7l2ud1NNMb5meyGj7NbDWcXgZWs00X7/PFnnoo7/xT7O9LcVU9B2U1HWd12mniMX865E7p/THS0XRtldL0fg3LNjjb/wCdd3VT6o5oTdMbvRfrFugAgAAAAAAAAAAg9Z2V0vWOFcvWuKyJ++tbqavXHNLOte2U1HRuFdqp84xfzrcckfqjobC/Ko3+j1rKMBGlbS7DWcrh5WjU0WL/ADzZ5qK+78M+zuZxesXse9XYyKK7d2id1VNUbpie2G5dHgBUAAAAAAAAAAAAHq1crs3Irs1zRXRO+KondMTHTEvIDU9kNraNWppw8+qmjNpjknmi9HXHVPYtjAaZqoqpqpqmKonfExyTEx0xPW1DYzaunVLdODn1xGbRHo1TycdEf6mLFW4BkAAAAAAAAAAAAAAAAAAEbrmsY2iafVlZPLVzW7cfWuVdUOjUc7G03BuZmXXwLVuOXrmeiIjpljmu6zk65qFWVf5KOa1b38lunqjt61k0fDVNSytVzq8zLr31180dFNPRER0OQG0AFAAAAAAAAAAAAB1aZp2VqmZRi4VHDu1+FMdMzPRBp2Bk6pmW8XEo4d2ufVEdMzPRDX9ntCxdCweKs+ndr3TduzHLXPwhm3FfHZvZrE0LH9Hddyq4+cvTHL3U9UJwGAAAAAAAAAAAAAAAAAQe0mzWJruP6W61lUR83eiOXuq64TgDCtRwMrTMyvFzaOBdo8JjomJ6YcradodCxddwZtXqeBdo38Ve3ctE/GP/ANzsg1HAydMzrmLl0cC7RPqmOiYnphuXRygNIAAAAAAAAAAAAPVFddu5FduqaKqJiYmJ3TExzTE9byA1fY3aeNZx/NcuqIz7ccvRF2I+1HxWhgmPfvYuRRfx65t3bcxVTVHPEw1/ZbaC1ruBwuSjKt7ovW+38UdksWYqcAZAAAAAAAAAAAAAAB5rrpopmu5VEUxEzMzyRERzzL0z3yg7R8OqrRcKv0Y/mKo6Z6KPjKyaIPa/aGvXM7gWapjCsTPFR+KemqfgrwNoAKAAAAAAAAAAAAD3Zs3L96i1ZomuuuYpppjnmZ5oh4aP5PtnuIsxrGXR87cj5iJ+zTPPV3z/APudLcVN7K7O29CwfS3V5l6Im9c/0x2QnQcwAAAAAAAAAAAAAAAAAAAAQW1WztvXcH0d1GZZiZs3Ov8ApnslOgMDvWbli9XavUTRXRM01UzzxMc8S8NG8oOz3H2atYxKPnbcfPxH2qY5qvV0/wCzOXSXQAVAAAAAAAAAAAAB2aTqWTpOoW8zGq3V0c8dFVM88T2OMQblpGpY2r6fbzMar0K+eOmiY54ntdrH9ktoK9D1L5yqZw726L1PV1VR2w16ium5biu3VE0zETExzTE80wxZivQCAAAAAAAAAAAD5ZORZxMW5k5FfAtW6ZqqnqiAQm1+vxoel/MzT53f302Y6uuqeyGQ1TNdU1VVTNUzvmZ55memXfr2q3tZ1S5mXuSmeS3T+CmOaPij3STAAVAAAAAAAAAAAAAAE5slony3rEUXKfotndXenrjfyU98z8WxU0000xTTTupjkiI5ohCbJaP8jaLbtXKd2Td+cvfqmOSPVHxTjnaoAgAAAAAAAAAAAAAAAAAAAAAA/KqYqpmmqmJpnkmJ5piehju1uifImsTRbp+i3t9dmeqN/LT3xPwbGg9rdHjWdGuWqafpFr5yz+qI5Y9fN4LLgxwB0QAAAAAAAAAAAAAAaB5PNoN+7RcuvrnHmfGafjHrZ+9Wrldm9Rds1TRXRMVU1RzxMc0pZqt9EPsxrNGuaTRk8kX6PRvUx0VR0+vnTDmAAAAAAAAAADO/KNrnGXI0fHq9GjdVkbumrnin4+C5bQ6rRo2j3syrdw4jdapn7Vc80f8A7qYrevXL96u7eqmuu5M1VVTzzMzvmWuM/o8ANoAAAAAAAAAAAAAALJsJpXylr1F25TvsYnzlXVNX2Y8eX1K21rYLTfMNnbd2qnddy542rr4PNTHh72bVWUBgAAAAAAAAAAAAAAAAAAAAAAAAAAZJt1pXybr1ddundYyvnKeqKvtR4+9W2tbe6b5/s7Xdpp33cSeNp/TzVR4cvqZK3AAaQAAAAAAAAAAAAABN7Ja3OiaxRXcq+i3t1F6P6eirvifi2OmYmnhU8tM8sSwFp/k+1vz7TZ07Ir338SPR389Vvo8J5PBjlFXABkAAAAAAARG0+q/I2h3sqn+LV6Fn9U80+rln1AoPlA1nz/WPM7NXzGJvp5Oaqueefh4qqVTM1TVVVvqnlmZ6Z6x0nxABQAAAAAAAAAAAAAB1aXhTqOqYuHT99dinujfyz4b250UUW7dNFundTREREdURG6PYzHybYXH65cyqqfRxbU7v1Vckeze1BjkoAyAAAAAAAAAAAAAAAAAAAAAAAAAAPNdFNy3VbuU76a4mJjrieSWGaphTp2qZOHV9xdmmO2N/JPhubqy/yk4XEa5byqafRyrUb/1U8k+zc1xFQAbQAAAAAAAAAAAAAAd2i6lc0nVLGdb5eLn0o/FTPJMeDhEG94963kY9u/Zq4dFymKqZ64mN8PopPk31jj8G5pV6r08f0rXbRM8seqfeuznfigAAAAADLfKJqvnesU4Nur5rEjdPbXPP4Ru9rRdYz6NM0vJzrn3NEzEddXNEeO5h925XevV3blW+uuZqqnrmZ3y1xg8gNoAAAAAAAAAAAAAAAA0/yaYvF6HeyftX78+FMRHvmVwQ+yVjzfZfT6ObfZiue+qZn4phzqgCAAAAAAAAAAAAAAAAAAAAAAAAAAAp/lLxeO0Ozk8H0rF+P/bVG6fbuXBD7W2PONl9Qo6rU1x30zE/BZ6MYAdEAAAAAAAAAAAAAAAAd+h6lXpOsY2dT9W3V85HXTPJMeDbqK6Llumu3VvpriJieuJjfE+DAmq+T7VPPtB82uVb7uFPA7eDPLTPvj1MclWoBkAAAAULymalus4umW6vrzxt3ujkpifXv8IZ6k9pNR+U9ezMrhehNfBt/pp5I/f1ox0ngAKgAAAAAAAAAAAAAAbt/o9fJ4j64lHGZlmjru0x4zANzwrfE4di1+C1TT4RD7g5KAAAAAAAAAAAAAAAAAAAAAAAAAAAAPhmW+Pw79r8y1VT4xMPuAwAfXLo4GZfo6rtUeEzD5OqAAAAAAAAAAAAAAAACxbDal8nbRWablXBtZUcVV1b5+rPj71dftMzRVFVNW6qJiYnqmOaUo34cOjZ0apo+LmU/fURM9lUckx4xLuc1AAEPtVn/JuzuZfpq3VzRxdv9VXJHx8EwoHlPzvRwtPpq599+r1ckfFZ6M/AdEAAAAAAAAAAAAAAAAHVpMcPVsKnrv24/wDKHK6tMngaph1U9F+if/KEG6gOagAAAAAAAAAAAAAAAAAAAAAAAAAAABT9YAYVq3Jq2b//AEXP/lLldWpzw9UzKuvIuT41S5XRABQAAAAAAAAAAAAAAABo/kzz+MwcrAqq9KzVFyn9NXP7Y9q8Mg2GzvMtqMbhVbqMjfYn183tiGvudUAQGO7a5vnu1GZ+GzMWqf7Y3T7d7Xcm9GPi3L9X1bdE1z6o3sHu3Jv3q7tz61yqap75ne1xHkBtAAAAAAAAAAAAAAAAB9Mevi8i3X1VxPhMPmA37fv5evlfrl0y95xpuLf/ADLNFXjEOpyUAAAAAAAAAAAAAAAAAAAAAAAAAAAABy6ne8303Kv/AJdmurwiQYdkV8ZkXK/x1zV4zMvmDqgAAAAAAAAAAAAAAAAAD3ZuV2L1F239a3VFUd8Tvhu2JfjKxbOTb+reoiuPXG9gzXdhMvzrZXGpq+tYmbU+qd8eyYZ5KsQDAgdtsrzXZXNq5qrkRaj+6YifZvY80nynZHA0vDxvzL01T3Ux+8s2b4+AA0gAAAAAAAAAAAAAAAAADYtisjzjZXBq+1bpm3P9szHu3J1SPJjl8PTczD+1ZuxXHdVG73wu7nVAEAAAAAAAAAAAAAAAAAAAAAAAAAABBba5Hm+yubV9q5TFuP7piPcnVI8p2XwNNw8On6167Nc91Mbo9s+xZ6M3AdEAAAAAAAAAAAAAAAAAAGg+S7K+b1DDqq5ppux698T7oZ8tPk7yOJ2mi1+fZrp9cbpj3JfFasA5jNfKdf4erYdj8uxNXrqq/wBlKWPb69xm1mTH5dNFHhTEz7ZVx0ngAKgAAAAAAAAAAAAAAAAAC0eTzN812ki1VV6OVRNv+6OWPd7WrsFxMivEyrOTb+vZriuO+J3t0xcijLxbOTZ+pepiuO6Y3sclfYBkAAAAAAAAAAAAAAAAAAAAAAAAAAGT+UHN862km1TVvpxaIt/3Tyz74j1NSy8i3iYt7JvfUs0zXPdEb2F5eRXl5V7JufXvVzXPfM72uI+QDaAAAAAAAAAAAAAAAAAACU2Yv+b7Tabd5vn4p9VXJ8UW+mPcmzkW66fsVxV4TEoN7HiJoriKvxco5qxjaq5xu1Go1/58x4bo+CKdWqV8Zq2bX137k/8AlLldEAFAAAAAAAAAAAAAAAAAABp/k51LzrR68G5V85iVcn6KuWPbv9jMExsrqvyPrlnJq/gV/N3v0z0+qd0+pL4rZx+U8r9cwAAAAAAAAAAAAAAAAAAAAAAAAB+cwKh5RtS810ejT7dXzmXVy9lFM75+HtZgmNqtV+WNev5NNXzFHzdn9MdPrnfKHdJPgAKgAAAAAAAAAAAAAAAAAA/Kvq1dz9AbppmVTXpWJV+KxRP/AIwKnpWqzRpGFT1WKI+tH4YHPFZ3kTvyLlXXVPvl8wdEAAAAAAAAAAAAAAAAAAAAAAarsFrPyjo/mt6r6TiRFPLz1UfZn4epamIaFqt7RtUs5lnl3clyn8dM88fFtONkWcvFt5OPXw7V6mKqZ64lzsxX2AQAAAAAAAAAAAAAAAAAAAAAAFV291v5N0nzWzV9Iy4mmOumjpn4etZMvIs4mLcycivgWrNM1VT1RDFtc1W9rOqXsy9yb+S3T+CmOaPismiPAdEAAAAAAAAAAAAAAAAAAAAAATVjImnHtx83yURHsEKIA9XY4FyunqmY8JeVAAAAAAAAAAAAAAAAAAAAAABdNgdovMsiNKy6/o96r5mqfsVT0d0+/vUsSwb+KdsPtP8AKNmnTs+v6Zbj5uqfvaY/1QuLmoAAAAAAAAAAAAAAAAAAAACnbcbT/J1mrTsCv6Zcj5yqPuqZ/wBUghNvtovPcj5KxK/o9mfnqo5q646O6Pf3KYDpIACoAAAAAAAAAAAAAAAAAAAAAADrt4tFVumeFXyxEiDxqFHF6hk0fgvVx4VTDnSO0VvidotQo6siv2zv+KOAAUAAAAAAAAAAAAAAAAAAAAAAerVyuzciu3VNFdExNMxyTExzTDVNkNqqNZtxi5dUUZ9Ed0XYjpjt64ZS9Wrldm5F23cmiuiYmmqJ3TEx0xPQlmq30VrY/XM/VcXi9Rw7tFdEcmRwJpouR8KllcwAAAAAAAAAAAAAAAABWdsdcz9KxeLwMO7VXXHLk8XNVFuOzrkHna/aq3o1urFxKorz6474tRPTPb1Qyq7crvXK7tyqa665maqp5ZmZ55mS7crvXKrtyua665mZqmd8zM9Mz1vLpJgAKgAAAAAAAAAAAAAAAAAAAAAAD8BeNO06uvTcWv0/Ss0T0dUC4aTp0Ro+FG7mx7f/AMYGNVm23FvidrM3+uaa/GmJQK3eUuzxe0Vu7+djx4xMx7tyotTwAFQAAAAAAAAAAAAAAAAAAB16dpubqeRxGBjV3a+ndzU9sz0Qg5HRh4OVn3uKwsa5fr6qY37u+ehfdG8ntm3wbus3+Nq/JtTMUx31c8rniYmNh2abGJYt2LVP2bcREetLyVn2l+TzKvbq9VyYsU/l2t1VXrnmhcNM2Z0bS91WPh0TXH3t306vGeZMDO0AEAAAAAAAAAAAAAAAAAAEPqezOkapvqyMOiLs/e2vRq8Y51P1TyeZVnhV6VkxkU/l3d1NXqnmn2NIF2jCMzBysC9xWbjXLFfVcjdv7p6XO3jLxMbNszYy7Fu/an7NymKoUzWfJ7Zub7ujX+Kq5+Ju75p7oq54anIZ0OrUdNzdMyOIz8euxX0b45Ku2J6XKqACgAAAAAAAAAAAAAAAAAA/Yjh1RT1zEePI/HdodjzjXMCx+O/RE92+Jn2INvsRFrHt2vwUxT4QP0c1UPyo2PmdPyeqqu3PriJj3M9ax5Qcfj9l7lf2rFyi56t+6fZLJ2+PgANIAAAAAAAAAAAAAAAAPdmzcv3ItWbddyuud0U0xMzM9kJXQNnM7XL30eni8eJ9K/VE8GOyOuWoaHs7p+h2fo1G+7Mbqr1XLVV+zNuKqegbAV18G/rVU0U88Y9ufS/uno9S+YmJjYOPFjEsUWrUc1NMbo/3fcZt0AEAAAAAAAAAAAAAAAAAAAAAAAAAAHwy8TGzceqxl2KLtqeemuN8f7KFr+wFdvhX9FuTcp5/N7k8v9s9PraILLgwO7buWbk2r1Fduuid001RMTE9sPDaNc2f0/XLPByaN12I9G9TyVU/uy/X9nM7Qr30injMeZ9G/TE8GeyeqWpdEOA0gAAAAAAAAAAAAAAAAsWwdjj9rMWfs2oqueEbo9sq6vHkwxeFnZuV+C1FuO+qd8+72pfFaOA5jj1fF890nMxefjrNVPr3cntYY39iW0mF5htBm43B3Uxemaf0zyx7Ja4iNAbQAAAAAAAAAAAAB6ooruXIot0zXXM7oiI3zMz0RHTIPK67L7EV5fAzNZprt2OemxzVVx11dUe3uS+yWxtGDwM7VaIryue3anlptds9dS5sWq+dmzbsWaLVmii3RRG6mmmN0RHVEPoDIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPnes279mu1eoouUVxuqpqjfEx1TD6AM12o2IrxOHmaRTNyxyzVY56qI66euPb3qU39S9rdjaM7h52lURbyue5ajkpu9sdVXvalGaD1XRXbuTRcpmiuid0xMbpiY54mOt5bQAAAAAAAAAAAAAAan5N8XiNnZv8H0si9NXqjdEe6WWNx0TE8w0XDxfy7NMT37t8+2ZZ5K7gGAZp5TMHi9Uxs6mn0b9rgT+qmf2mPBpatbfYPnuzNy5TTvrxaoux3RyT7J3+pZ6MlAdEAAAAAAAAAAAftMTXVFNNMzVPJERyzMz0QD9tW671yi1ZomuuuYimmI3zMz0RDVNkNlLekW6cvNpi5n1x3xZieiO3rl42M2WjSbMZ2bTvzbkc35NM9EdvWtjFqgDIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAqu1+ylvV7dWXhUxRn0R3Rejqnt6pZZdt12bldq9RNFdEzFVMxumJjniYb6qe2Wy0atZqzMKiIzaI5Y5uOpjont6mpRlg/aomiqqmqmYqjkmJ5JiY6Jh+NoAAAAAAAAAAAAlNmMH5R2iwsbg76eNiqr9NPLPubWzryY4PDyszUKqf4dMWqe+eWfZEeLRXPl6oAgPnes0X7Ny1c5aLlM0z3TG6X0AYRqGJXg6hfw7n1rFc0T6p5J8OVzrl5SdO4jVrOoU0+hlU7qv1U8nu3eCmukQAUAAAAAAAAGibB7McVTRrGfR87Vy49ufsxP2pjr6kLsPs58rZnnmXb+h4880/eVRyxHd1tVY5VQBkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUTbzZjjaa9YwLfp08uRbj7UfiiOvrZ239lO3GznyTmeeYlv6HfnmjmtVTz093U1xoqwDaAAAAAAAAAJLZ3TvlXXMXD+xNW+5+mOWfZ70Go7H6d8nbN4tFVO6u5HG3O+rlj2boTgOagAAAIPbDS/lPZ2/app33bUcba/VTyzHrjexxv7GtrdK+SdoL9qmndYufO2v01Tzeqd8NcaIUBtAAAAAAB26Rpt7VtSs4Nj61yeWroppjnmfU4mqbBaH8m6X55fo3ZWXETy89FHPEfHwS3FWLT8Kxp2Daw8andatxujrnrmeud7pBzAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABzahhWNRwb2Hk077V6ndPXHVMevldIDDdX02/pOpXsO/9a3PJPRVTPNMepxNV290P5S0vzzHp+lYkTPJz10c8x8fFlTpLoAKgAAAAAA0PyZ6bwLOTqtyn0q54q1+mOWqY9e7wUDHsXMrIt2LNO+u5VFNMdszubjpmDb03TbGHZ+rZoinvnpnx3s8qrqAYAAAABVPKDpHn2i+dWad9/D31d9E/Wj3T6lreaqaa6Zpqp30zG6YnpiegGBCV2l0mdG1q9i/dT6dmeuieb9vUinRABQAAABPbG6N8sa1RTcp+jWN1y727p5KfXPxbCr2xOk/Jeg26rlO6/kbr13rjfHJHh75WFztUAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGPbZaP8j65XTZp3Y9/5y12RPPHqn4NhV7bXSflXQbnF077+P87b653Ryx4e6FlwZCA6IAAAAA92bNy/eosWaeHXcqimmI6Zmd0QC4+TjSfONQuanep9DH9G121zHLPqj3tLcGi6bb0nSbGDb+7p9KfxVTyzPi73OqAIAAAAAAKxt3o3yno/nNmnfkYm+uN3PVT9qPj6mTt/ZDtrofyPrE1Wad2Lkb67XVTPTT4+9rjRXgG0AAExsppfyrtBjY1VO+1RPGXf008u7x3R60O0nyaadxOm39RuU+lkVcCn9NPP7d/gl8VdgHMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYxtVpfyTtBk2Kafmq54y1+mrl3eO/wQ7SfKXp3HabY1G3T6VirgVfpq5vb72bOk8ABUAAF48nGjcblV6tkU+hY302d/TVMcs+qPeqOmYN7UtQs4eP8AXvVbuyI6Zns3Ns0/Cs6dg2cPHp3WrNMUx29cz27+VnlVdIDAAAAAAAAAIvaHR7et6Tcxat0V/WtVfhqjmnu6PWlAGCXrNzHvV2L1E0XbdU01UzzxMc8Pm0Pyh7P8ZT8tYtPpUREZER009FXwn1dTPHSXQAVCmJrqimnlqnkjvluek4cadpOLh0/c2opnv3cs+9kuyGH59tNhWquWmirjKu6mN/v3NmY5KAMgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADj1bCp1HScrDq++tTTHfu5J8dzDKomKppq5Ko5Jjtjnb+xna/D8x2mzbVNO6mu5xlPdVG/3zLXEQwDaALHsZoHyzqXG5FP0PHmJuf11dFPxn/dBa/J/oXmOD8o5NH0jKj5uJ56LfR48/guIOagAAAAAAAAAAAPNdEV01UXKYmmYmJieaYnniWQbXaBVoepfM01Th399VmerrpnubC4dY0zG1jT7uHlU+jXyxV00VRzTCy4MOHXqmnZOlahcw8mndXbnn6Ko6JjscjaLv5MMXh6hm5VX3dqKI76p3z7va0hT/JpY4vQb1/8AOyJ8KYiFwYvqgCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAzfyn4vA1DCyvzLU0T30zvj3tIU7yl2OM0Gxf+1ayI8KomJ9sQs9GYg/aYmuqKaaZmqZ3REcszM9EQ6I6dMwL+qahZw8anfXcnd2RHTM9m7lbRpGm2NI023h431aI5Z6a6p55nt3ojY3Z2nRcHjcimPPb8Rxk/gjopj4/wCyyOdqgCAAAAAAAAAAAAAACA2s2do13B+b3UZlnfNmuenrpnslkV6zcsXq7V6iaK6JmmqmeSYmOeJb4qO2uy3yrZq1DAo+m249KmPvqY6P1dTUoiNgdpLePu0fL3UUV1TNi5zelPPTPr5misAqiYq4NW+Ko9UxMNI2J2r87po0zU6/pEclm7P3kdUz+L395YLuAyAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACA24s8dsnm/0RTX4VRKdqqpopmqqqIpiN8zPJERHTvZbtltVOrXpwcKqYwaJ5Z5uOmOmf6epYKo0PYLZjiaaNYz6PTmN+PbmPqxP2pjr6kdsTsr5/co1PUaPotE77VufvZjpn+n39zTVtABkAAAAAAAAAAAAAAAAAAUjbXZLzzh6nplv6Rz3rUfef1R/V7+9nHLRV0xVE90xMfFvykbZ7H+dcPUdKo+kc92zH3nbEfi9/e1KPrsbtdTn00afqdcRmRyW7s8kXuz9Xv71yYD6VFXTFUT3TEx7paLsftlTk8DT9XriL/JFq/PJFfVFU9FXv7ywXkBkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHmqqmimaqqoimI3zM8kREdO9+Xr1uxZru3q4t0URvqqqndERHTMsu2u2tr1aqrDwKq6MKOeeab09c9VPYsmj6bY7WfKfC0/TqpjDifnLnNN6Y91PvfLY/ZWvVrlOZm0zRgUTyR03pjoj+nrNkdlK9WuRmZtM0YETyRzTenqjqp7WpWrdFm3Ras0RRboiIppiN0REc0RC24P2iiLduKLdMUUxG6IiN0REc0RD0DIAAAAAAAAAAAAAAAAAAAAAAp+1+x9GpcPO0ymKMznqt81N7t7KvezK7brt3JouUzRXRO6aZjdMTHRMN9VzajZXG1y3N23usZtMcl3or6oq6+9qUVvZLbWcXgYOs1zXY5It355Zo6oq647Wi0V010xVbqiaZjfExO+JiemJYVn4OVp2VXi5tqbV2jonpjriemE1sxtXlaJVFi9vv4W/ltdNG/ppno7vcWDXRy6dqOLqeLGThXYu2p6ueJ6pjol1MgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA5tQzsbTsOvKy7sW7VHTPTPVEdKP2h2iwtCx+Ffq4y/MfN2KJ9Krtnqhler6xna7mRcyapr5d1qzRv4NO/opjplZNHbtPtRk67e4FO+1g0T6Nrf9btq65SeyGx1efwM/VaJoxee3ankqu9s9VPv7klsnsTFngZ2tW4mvnt488sU9U1dc9i9rb/IPNFFNFMUW6YimI3RERuiIjoiHoGQAAAAAAAAAAAAAAAAAAAAAAAAABG63ouFreLxGbb9KP4dyOSqieuJZVr+zmdoV756njMeZ+bv0xyT2T1S2d871m3fs12r1FFyiuN1VNUb4mO2FlwYjpOq5uj5XnOFd4FX2qeemuOqY6WobO7W4Wt002rm7HzPyqp5Ku2melXNpNhK7PDytF33KOecaeWqn9M9Pd71Hqiu3c4NW+iuie2JiY90tfKN9GZ7P7d38TgY2r8PIsc0Xo/iUx2/ij297Q8LNxc/HpycK/RdtT00z7JjolmzB0gIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIvWdd0/RbPCzbvpzHo2qeWurujogEnVO70quRSdpNu7ePw8XRt127zTfnlpp/TH2p9neq+0O1uoazvtcLzfD/ACaZ+t+qel9Nndj83Wd1+9vxsP8AMmPSrj+mPj72sz0RWNi6jrupTTZpuZORcnfVVM793bVPRDTNmtlMXRaYv3t1/N3ct3d6NHZTHR3pfS9LwtJxfNsKxFujpnnqqnrmemXaloAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACD1/ZjT9cpmuunicrdyX7ccv8AdH2oTgDF9c2c1HRbn0m1w7G/kv2+Wme/qnvcenalm6XkcfgX67VfTu+rV2THTDcq6KLlM0XKYrpnkmJjfEx2wpmu7A4uVvv6RVGNd5+Kq3zbnu/C1OX6PWh7fYeVusarTGLd5uNjlt1d/wCFcaK6LlMV26orpnliYnfE90sN1HS83S8jis/GrtVdG/6tXbE9Lo0jX9T0ar6FfmKOm1V6VE+roLx/BtgqGjbe6dl8G3qNPmd3rn0rc+vo9a2WrlF63Fdu5FdE81VMxMT3SyPYAAAAAAAAAAAAAAAAAAAAAAAAAAAAObMzsXAszfzb9uxR11Tu8I6ZB0ubNzcXT8eb+bfosWo6ap3b+yI6VJ1nyhxy2tGsb/8AOvR7qf3UzIyNR1nMjjq7+XkV8lMctU90R0Q11Ft1zygXLnCsaLb4unm4+5HpT+mno9ap4mJqOuZ0049F3KyK531VTO/11VdELVofk/vXt1/WLk2qPyLcxwp756F+wcHF07HpsYVii1ajopjn7Znpk2TwVnZ7YbEwOBk6nwMvI54p+7onu+0t4MgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD45OLYy7M2Mqxbu2p56bkRMKXrPk9s3N93Rr/FVfk3ZmafVVzwvQaMN1HSs/SrnAz8a5a6pnlpnunml+6ZrGoaVc4WBlXLXXTz0z308zbrtm3ftzavUUXKJ56aoiYn1Krq2wOmZe+5hVV4d3qj0qPDoa7QR+l+UWid1Gr4s0f5tjlj109C4adq2n6nTwsDKtXeyJ9KO+OeGWapsfrWnb6vNvOLUfeWPS8Y54QVM127nCpqmiujpjfTVE/Bcg30ZDp22muYG6mrJjJoj7N+OFP/u51p0/yi4N3dTn4t3Hq/Fb9On94Zyi6jgwdZ0zUf5TOsXKvw8KIq8J5XegAAAAAAAAAAAAAAAAA/Jq3U76uSOueYH6IbUNp9F07fTfz7c1x93a9OfCFZ1Dyjxy06Zgb+q5fn/TH7rlF/Q+p7TaNpe+nIzKJrj7q16dXhHMy7U9pNX1TfTk5lcUT93b9GnwjnedM2f1bVf5TDuTR+ZV6NPjPOvUWDVvKFm399GmWIxaPzLm6qv1RzQq1U5+rZnpecZmRX31Vf7QvWl+TqzRwa9Vypu1flWfRp9dXPK4YOBh6dZ4rCxrdij+mN2/vnpNk8Gf6N5P8q/wbur3fN6Pyre6que+eaF70vR9P0m3xeBjUW+urnqq756XeJtABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAR+o6JpmqU/TcO1cq/Fu3VeMcqQAUXUfJzjV+lpmZXa/wAu9HCp8edWdQ2O13B31eZ8fR+KxMVeznbAL2owK7brs3ODeort1x0VRNMpDD1/V8Hd5tqORRTH2Zq4VPhLZsnExsungZWNav09VymKkFl7D6FlelTj149X+TXMR4cq9hVMTyh6tZ3edWMbIp7poq9n7JnG8o2BX/M4ORa7aZiuPg5snybR/gtT9V618Y/ZD5OwOu2f4dOPf/5d3dPhO4+C8Y+2Wz9//H8XV1XaKqfakrOq6bkfy+fjXP0XqWQ5GzutY/8AG0zK9VE1R4xvR12xct1cG9Yroq/qomDqN6iqmr6vL3cr0wOi/ct/w79dHdXMOu1rGqWf4eo5lHdeqOo3EYvTtPrtH/Fcr11b30p2u2hp/wCK3fXTTPwOo2QY5VthtDP/ABO5/wCyj/6vnVtVr0/8VyPZHwOo2cYpXtHrlf1tVy/VXMOS7qWfc/jZ2RX33qv3Oo3Ou5bt/wASuij9UxDgyNf0bH/janiR2cbEz4QxX5y9+O541OzH0TVsj+X07Kr7YtTEeJ1Gl5O3Og2Pq5Ny/V1WrU++dyHyfKTb5fNNMrntu1xHshAY+w+0F/dwsWix/wA27EeyN6XxvJxkz/N6jao7LVE1T4zuMgjszb7XMj0bNVjGp/y6N9XjO9AZepZ+dV9LzL9+qeiquZ8IaVibAaLY3cd5xk1f5lzgx4QnsLSNOwf5TBx7XbTRG/xNgyLT9m9Zz93mun3eBP2rkcCnxlZtO8nN6rg1annUUU/l2I4U93ClogdqIXTNldF03dVZw4uVx95e9Or1dSaBkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH5VEV0elEVd/KAOLO07Aqt+lhY099qn9lW1bTNPpmeDgYsc/NZp6u4FgqeVYs01clm3HJ0UwgwbBI4Fq3VTb4VumeXpjtAFo0jT8G5weMw8ern+tapn4LhpmmafEcmBix/wBGn9gZolKbVu1T81bpo/TG56BkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf/2Q=="

   
   var video = $(`<video id="video" class="capture" playsinline webkit-playsinline poster="${poster}"  width="${width}" height="${width}"   muted="true"></video>`);
    var canvas = document.createElement("canvas");
    canvas.width = width;
    canvas.height = width;
    $('video').remove();
    $('body').append(video);
    $('body').append('<h3>请正对摄像头</h3>');
  
    let ctracker = new cml.tracker();
    ctracker.init();
    ctracker.start($('#video')[0]);
  
    install($('#video')[0], function () {
       
            getface();
            function getface() {
                var positions = ctracker.getCurrentPosition();
                if (positions) {
                    //  type 存在处理各种信息验证
                    if (type && type.length > 0) {
                        if (tree(type,$('h3'),positions)) {
                            getimg();
                        }
                    }else{
                           getimg();
                    }
                }
                requestAnimationFrame(getface);
            }
                function getimg() {
                    canvas.getContext('2d').drawImage($('#video')[0], 0, 0, canvas.width, canvas.height);
                    let img = canvas.toDataURL("image/png");
                    plus.storage.setItem("facetempuserimg", img);
                }
            },
            function () {
              
            });
    }