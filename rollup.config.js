import babel from "rollup-plugin-babel";
// import resolve from 'rollup-plugin-node-resolve';
// import commonjs from 'rollup-plugin-commonjs';
import css from "rollup-plugin-import-css" ;
// import json from 'rollup-plugin-json';
// import { uglify } from "rollup-plugin-uglify";


export default {
    input: 'src/index.js',
    output: {
      file: 'dist/bundle.js', // rollup支持的多种输出格式(有amd,cjs, es, iife 和 umd)
      format: 'umd',
     
    },
    // globals: {
    //     jquery: '$'
    // },
    //  external: ['jquery'],
    plugins: [
      babel(),
      // resolve({
      //   module: true,
      //   main: true
      // }),
      css()
      // commonjs()
      // json()
    ]
}